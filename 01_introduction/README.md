<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction

- Objective: Achieve a basic understanding of software processes and change management
- Estimated time: 90min

## Content

- [Introduction to software processes & change management](slides/introduction-to-software-processes-and-change-management.pptx)
- Exercise about participant's current way of developing software

## Further Readings

- [DLR Software Engineering Guidelines (Change Management)](https://rse.dlr.de/guidelines/00_dlr-se-guidelines_en.html#aenderungsmanagement)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
