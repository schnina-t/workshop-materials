<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Example Project

In the following, we demonstrate different GitLab features in context of a software project:
- You can find related slides in the [slides directory](slides).
- You can find related code examples in the [code directory](code).
- You can find the complete example project showing all important steps here: https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator 
  (branch `02a-initial-design` is the starting point for iteration 2,
  branch `03a-prepare-iteration-3` is the starting point for iteration 3).

## Set the Scene

- [Explain the project context and our tasks](episodes/episode01.md)

## Iteration 2 - Starting in the middle of the Project

- [Set up the issue tracker](episodes/episode02.md)
- [Introduction to build automation](episodes/episode03.md)
- [Set up the build pipeline](episodes/episode04.md)
- [Close iteration 2](episodes/episode05.md)

## Iteration 3 - Finish the initial Release

- [Perform iteration 3](episodes/episode06.md)
- [Release version 1.0.0](episodes/episode07.md)

## Bonus

- [Bugfix releases using release branches and tags](episodes/episode08.md)

## Further Readings

- [DLR Software Engineering Guidelines](https://rse.dlr.de/guidelines/00_dlr-se-guidelines_en.html)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
