<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Workshop Materials

This workshop builds upon the HIFIS Workshops [Introduction to Git and GitLab](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material) and [Bring Your Own Script and Make It Ready for Publication](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials)
but focuses on the collaboration aspects of Git and GitLab.
You will get a deeper understanding on Git concepts such as branch, merge and rebase, which are necessary to know if use Git in a team setup.  

In addition, you learn further concepts and practices for software development in teams to improve the change process of your software.
In this context, we make use of GitLab features such as issues, merge requests, and build pipelines.
You learn how to apply these features on the basis of a [consistent example project](https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator) which follows an iterative development approach.

## Prerequisites

- Basic knowledge about Git and a collaboration platform such as GitLab or GitHub.
- Basic knowledge in using a programming language such as Python, R, or Matlab.

## Setup

- Bring your computer including a Web browser, a text editor, and a Git client.
- GitLab account

## Curriculum

- [Part 1: Introduction](01_introduction/README.md)
- [Part 2: Git for Teams](02_git-for-teams/README.md)
- [Part 3: Example Project](03_example-project/README.md)

## Schedule

The actual schedule may vary slightly depending on the interests of the participants.

### Day 1

| Time | Topic |
| ------ | ------ |
|  | [Setup](#Setup) |
| 09:00 - 09:30 | Welcome & Introduction |
| 09:30 - 11:00 | Introduction to Software Processes & Change Management |
| 11:00 - 12:00 | Git for Teams - Preparation |
| 12:00 - 13:00 | *Lunch Break* |
| 13:00 - 14:30 | Git for Teams - Branching / Merging / Rebase |
| 14:30 - 16:45 | Git for Teams - Conflicts / Merge Requests |
| 16:45 - 17:00 | Wrap Up & Feedback |

### Day 2

| Time | Topic |
| ------ | ------ |
| 09:00 - 09:15 | Welcome & Introduction |
| 09:15 - 10:30 | Example Project - Issue Tracking |
| 10:30 - 12:00 | Example Project - Build Pipeline |
| 12:00 - 13:00 | *Lunch Break* |
| 13:00 - 15:30 | Example Project - Team Exercise |
| 15:30 - 16:45 | Example Project - Release Management |
| 16:45 - 17:00 | Wrap Up & Feedback |

## Contributors

Here you find the main contributors to the material:

- Tobias Schlauch
- Martin Stoffers
- Michael Meinel

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes of the material.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
